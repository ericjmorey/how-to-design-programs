# How to Design Programs

A repository of my solutions and notes to the exercises in the online textbook How to Design Programs How to Design Programs, Second Edition by Matthias Felleisen, Robert Bruce Findler, Matthew Flatt, Shriram Krishnamurthi (https://www.htdp.org/)

This is one of my first attempts at learning the basics of computer programming. My approach is to get working solutions for each exercise (most having their own file) with occational exploration of curiosities to keep myself engaged.

If you find this useful send me a message or whatever.

Gitlab provides me with the following email address but my tests have all bounced back as undeliverable. 

1984911-ericjmorey@users.noreply.gitlab.com

You can also try my first name at my full name with middle initial .com 

(Hopefully the computers can't figure that out and give me even more spam)