;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname htdp.0028) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
; HtDP Exercise 28

#| Exercise 28. Determine the potential profit for these ticket prices:
 $1, $2, $3, $4, and $5. Which price maximizes the profit of the movie
theater? Determine the best ticket price to a dime. 
|#

(define BASE-ATTENDANCE 120)
(define BASE-PRICE 5.0)
(define DELTA-ATTENDANCE 15)
(define DELTA-PRICE 0.1)
(define VARRIABLE-COST-COEFFICIENT 0.04)
(define FIXED-COST 180)

(define ticke-price 5.00)

(define (attendees ticket-price)
  (- BASE-ATTENDANCE (* (- ticket-price BASE-PRICE)
                        (/ DELTA-ATTENDANCE DELTA-PRICE))))

(define (revenue ticket-price)
  (* ticket-price (attendees ticket-price)))

(define (cost ticket-price)
  (+ FIXED-COST (* VARRIABLE-COST-COEFFICIENT (attendees ticket-price))))

(define (profit ticket-price)
  (- (revenue ticket-price)
     (cost ticket-price)))

#| Determine the potential profit for these ticket prices:
 $1, $2, $3, $4, and $5.|#

(profit 1)
(profit 2)
(profit 3)
(profit 4)
(profit 5)

#|Which price maximizes the profit of the movie
theater?
10101010 denotes no maximum found, aka an error in the code|#

(cond
  [( and
     (> (profit 1)
            (profit 2))
     (> (profit 1)
            (profit 3))
     (> (profit 1)
            (profit 4))
     (> (profit 1)
            (profit 5)))
   1]
  [( and
     (> (profit 2)
            (profit 1))
     (> (profit 2)
            (profit 3))
     (> (profit 2)
            (profit 4))
     (> (profit 2)
            (profit 5)))
   2]
  [( and
     (> (profit 3)
            (profit 2))
     (> (profit 3)
            (profit 1))
     (> (profit 3)
            (profit 4))
     (> (profit 3)
            (profit 5)))
   3]
  [( and
     (> (profit 4)
            (profit 2))
     (> (profit 4)
            (profit 3))
     (> (profit 4)
            (profit 1))
     (> (profit 4)
            (profit 5)))
   4]
  [( and
     (> (profit 5)
            (profit 2))
     (> (profit 5)
            (profit 3))
     (> (profit 5)
            (profit 4))
     (> (profit 5)
            (profit 1)))
   5]
  [else 10101010])

#| Determine the best ticket price to a dime.|#

(profit 2.9)

(if (> (profit 2.9) (profit 3)) 2.9 3)
