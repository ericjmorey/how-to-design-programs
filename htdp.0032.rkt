;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname htdp.0032) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
#| HtDP Exercise 32.

Most people no longer use desktop computers just to run applications but
also employ cell phones, tablets, and their cars’ information control screen.
Soon people will use wearable computers in the form of intelligent glasses,
clothes, and sports gear. In the somewhat more distant future, people may
come with built-in bio computers that directly interact with body functions.

Think of ten different forms of events that software applications on such
computers will have to deal with.
|#

1 touch screen inputs
2 fingerprint recognition
3 video input
4 incoming call signal
5 gps signal
6 accelerometer signal
7 send/recieve text
8 application notice
9 Time/countdown alarm
10 File Creation/Download